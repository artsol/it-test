# intterra-test

## Архитектура
К стандартной архитектуре Vue приложения добавлена сущность "сервис".
Подразумевается следующая структура.
- На каждую сущность данных приложения создается модуль store и Service.
- Сервис получает данные из api-сервиса, обрабатывает ошибки запроса, логирует запросы/ошибки, сохраняет данные в vuex-store... Короче, занимается обеспечением получения и хранения данных, и является своеобразным "фасадом" для работы с данными из компонентов.
- У каждого сервиса обычно есть метод prepare, который получает и подготавливает данные конкретной сущности для отображения в компонентах.
- Публичные методы сервиса ничего не должны возвращать.
- Vuex store mutation могут вызываться только из Vuex store Action.
- Vuex store action могут вызываться только из сервиса.
- методы api сервисов могут вызываться только из сервисов
- Данные в компонентах получаем исключительно из vuex store getter.
- Публичные методы сервиса доступны в компонентах.

## Иконки
Иконки собираются в шрифт из папки icons, и генерируется соответствующий scss файл со стилями из шаблона ```iconFontScssTemplate```

P.S. иконки пшеницы из макета генерируются криво, потому что скомпозированы криво. Исправить пока не успел.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
