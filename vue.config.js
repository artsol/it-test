/* eslint "@typescript-eslint/no-var-requires": 0 */
const IconfontPlugin = require("iconfont-plugin-webpack");
module.exports = {
  transpileDependencies: ["vuex-module-decorators"],

  configureWebpack: {
    plugins: [
      new IconfontPlugin({
        src: "./src/assets/icons", // required - directory where your .svg files are located
        family: "iticons", // optional - the `font-family` name. if multiple iconfonts are generated, the dir names will be used.
        dest: {
          font: "./src/assets/fonts/[family].[type]", // required - paths of generated font files
          css: "./src/assets/scss/_[family].scss" // required - paths of generated css files
        },
        watch: {
          pattern: "src/assets/icons/**/*.svg", // required - watch these files to reload
          cwd: undefined // optional - current working dir for watching
        },
        cssTemplate: require("./iconFontScssTemplate") // optional - the function to generate css contents
      })
    ]
  },

  pluginOptions: {
    i18n: {
      locale: "intterra-ru-RU.json",
      fallbackLocale: "intterra-ru-RU.json",
      localeDir: "locales",
      enableInSFC: false
    }
  }
};
