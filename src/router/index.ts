import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ErrorView from "@/views/ErrorView.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: HomeView,
    redirect: "/scheduled"
  },
  {
    path: "/:operationStatus",
    name: "ScheduledOperations",
    component: HomeView
  },
  { path: "*", name: "404", component: ErrorView },
  { path: "/404", name: "page404", component: ErrorView }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
