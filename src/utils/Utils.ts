export default class Utils {
  /**
   * Пытается привести строку к перечислению. Возвращает соответствующий член перечисления либо null, если совпадений не найдено
   * @param enumObject Перечисление
   * @param value Строка
   */
  public static tryCastStringToEnum<T extends { [k: string]: string | number }>(
    enumObject: T,
    value: string
  ): T[keyof T] | null {
    if (!this.someEnum(enumObject, v => v === value, false)) return null;

    return value as T[keyof T];
  }

  /**
   * Аналог метода some массива для перечисления
   * @param enumObject Перечисление
   * @param predicate Предикат для some
   * @param isNumeric Является ли перечисление числовым (`true` по умолчанию) — для числовых перечислений Typescript
   * подставляет в результирующий объект в качестве названий полей и ключи, и значения перечисления, и для числового
   * перечисления без этого флага предикат будет вызван для ключей вместо значений
   */
  public static someEnum<T>(
    enumObject: T,
    predicate: (val: T[keyof T], key: string) => boolean,
    isNumeric = true
  ): boolean {
    const enumValue = function enumValue<T>(
      enumObject: T,
      key: string
    ): T[keyof T] {
      return (enumObject as any)[key];
    };

    return Object.keys(enumObject).some(key => {
      const val = enumValue(enumObject, key);
      return (typeof val === "number") === isNumeric && predicate(val, key);
    });
  }
}
