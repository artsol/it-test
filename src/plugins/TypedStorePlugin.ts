import _Vue from "vue";
import { Store } from "vuex";
import { initializeStore } from "@/store/typedStore";

export default function TypedStorePlugin(
  Vue: typeof _Vue,
  options: { store: Store<any> }
) {
  const typedStore = initializeStore(options?.store);
  Vue.prototype.$tstore = typedStore;
}
