import _Vue from "vue";
import { initializeServices } from "@/services";
import { Store } from "vuex";

export default function ServicesPlugin(
  Vue: typeof _Vue,
  options: { store: Store<any> }
) {
  const services = initializeServices(options?.store);
  Vue.prototype.$services = services;
}
