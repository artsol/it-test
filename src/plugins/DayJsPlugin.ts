import _Vue from "vue";
import dayjs from "dayjs";
import "dayjs/locale/ru";

dayjs.locale("ru");

export default function VueDayJsPlugin(Vue: typeof _Vue) {
  Vue.prototype.$dayjs = dayjs;
}
