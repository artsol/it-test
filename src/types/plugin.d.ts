import { Vue as _Vue } from "vue/types";

export type PluginFunction<T> = (vue: typeof _Vue, options?: T) => void;

export interface PluginObject<T> {
  install: PluginFunction<T>;
  [key: string]: any;
}
