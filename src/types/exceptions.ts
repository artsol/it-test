enum ExceptionType {
  UNKNOWN,
  CRITICAL
}

export class Exception extends Error {
  public readonly excType: ExceptionType = ExceptionType.UNKNOWN;

  constructor(public message: string, public internalError?: any) {
    super(message);
  }
}

export class CriticalException extends Exception {
  public readonly excType: ExceptionType = ExceptionType.CRITICAL;
}

/**
 * Исключение для отслеживания недопустимых ситуаций на этапе компиляции
 */
export class NeverException {
  constructor(private value: never) {}
}
