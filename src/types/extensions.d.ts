import { TypedStore } from "@/store/typedStore";
import { Services } from "@/services";

declare module "vue/types/vue" {
  import { Dayjs } from "dayjs";

  interface Vue {
    $tstore: TypedStore;
    $services: Services;
    $dayjs: Dayjs;
  }
}
