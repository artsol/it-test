import { Store } from "vuex";
import { initializeStore } from "@/store/typedStore";
import OperationService from "@/services/OperationService";
import FieldService from "@/services/api/FieldService";

export interface Services {
  operations: OperationService;
}

export const initializeServices: (store: Store<any>) => Services = (
  store: Store<any>
) => {
  const typedStore = initializeStore(store);
  const operations = new OperationService(typedStore, new FieldService());
  return { operations }; // fieldService is api service. It shouldn't be accessible from anywhere exclude services.
};

export * from "./OperationService";
