import FieldService from "@/services/api/FieldService";
import Operation, { Assessment, OperationType } from "@/models/Operation";
import { TypedStore } from "@/store/typedStore";
import Service from "@/models/Service";

export default class OperationService extends Service {
  constructor(private store: TypedStore, private fieldService: FieldService) {
    super();
  }

  // private firstOperationId = operations[0].id;

  private async requestOperations(): Promise<Operation[] | void> {
    try {
      return await this.fieldService.getOperations();
    } catch (e) {
      return this.handleError(e as never);
    }
  }

  private async saveOperation() {
    const newOperation = new Operation({
      id: null,
      type: OperationType.HARVESTING,
      date: { year: 2018, month: 9, day: 25 },
      area: 85.5,
      comment: "New Test Comment",
      assessment: Assessment.SATISFACTORILY
    });
    try {
      return await this.fieldService.saveOperation(newOperation);
    } catch (e) {
      return this.handleError(e as never);
    }
  }

  private async requestOperationById(id: string) {
    return this.fieldService.getOperation(id);
  }

  public async prepareOperations() {
    let operations: Array<Operation>;
    try {
      operations = (await this.requestOperations()) || [];
    } catch (e) {
      return this.handleError(e as never);
    }
    this.store.operations.setOperationsAction(operations);
  }
}
