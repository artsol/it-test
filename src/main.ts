import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import ServicesPlugin from "@/plugins/ServicesPlugin";
import TypedStorePlugin from "@/plugins/TypedStorePlugin";
import "@/assets/scss/index.scss";
import i18n from "./i18n";
import VueDayJsPlugin from "@/plugins/DayJsPlugin";

Vue.config.productionTip = false;
Vue.use(ServicesPlugin, { store });
Vue.use(TypedStorePlugin, { store });
Vue.use(VueDayJsPlugin);

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
