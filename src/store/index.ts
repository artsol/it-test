import Vue from "vue";
import Vuex from "vuex";
import OperationsModule from "@/store/OperationsModule";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {},
  mutations: {},
  actions: {},
  modules: {
    operations: OperationsModule
  }
});
