import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import Operation, { OperationStatus } from "@/models/Operation";
import { toArray } from "lodash";

export type OperationsMapByStatus = Map<OperationStatus, Operation[]>;

@Module({ name: "operations" })
export default class OperationsModule extends VuexModule {
  private _operations: Array<Operation> = [];

  @Mutation
  private setOperationsMutation(operations: Array<Operation>): void {
    this._operations = operations;
  }

  @Action({ commit: "setOperationsMutation" })
  public setOperationsAction(operations: Array<Operation>): Array<Operation> {
    return operations;
  }

  public get operations(): Array<Operation> {
    return this._operations;
  }

  public get operationsMapByStatus(): OperationsMapByStatus {
    return this.operations.reduce(
      (result: OperationsMapByStatus, nextOperation: Operation) => {
        const [year, month, day] = toArray(nextOperation.date);
        const status =
          new Date().getTime() - new Date(year, month, day).getTime() > 0
            ? OperationStatus.COMPLETED
            : OperationStatus.SCHEDULED;
        if (result.has(status)) {
          result.get(status)?.push(nextOperation);
        } else {
          result.set(status, [nextOperation]);
        }
        return result;
      },
      new Map<OperationStatus, Operation[]>()
    );
  }
}
