import { Store } from "vuex";
import { getModule } from "vuex-module-decorators";
import OperationsModule from "@/store/OperationsModule";

export interface TypedStore {
  operations: OperationsModule;
}

const initializeStore: (store: Store<any>) => TypedStore = (
  store: Store<any>
) => ({
  operations: getModule(OperationsModule, store)
});

export { initializeStore };
