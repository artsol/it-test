type ErrorHandler = (e: never) => void; // TODO: make class

class Logger {
  log(...args: Array<any>): void {
    if (process.env.NODE_ENV !== "production") {
      console.log(new Date().toISOString(), ...args);
    }
  }

  error(...args: Array<any>): void {
    if (process.env.NODE_ENV !== "production") {
      console.error(new Date().toISOString(), ...args);
    }
  }

  warn(...args: Array<any>): void {
    if (process.env.NODE_ENV !== "production") {
      console.error(new Date().toISOString(), ...args);
    }
  }

  info(...args: Array<any>): void {
    if (process.env.NODE_ENV !== "production") {
      console.info(new Date().toISOString(), ...args);
    }
  }
}

export default class Service {
  constructor(errorHandler?: ErrorHandler, logger?: Logger) {
    this.handleError = errorHandler || this.handleError;
    this.logger = logger || new Logger();
  }

  private logger: Logger;

  handleError: ErrorHandler = (e: Error) => {
    this.logger.error(e);
    throw e;
  };
}
