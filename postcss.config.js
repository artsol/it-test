/* eslint "@typescript-eslint/no-var-requires": 0 */
module.exports = {
  plugins: [
    require("tailwindcss")("tailwind.config.js"),
    require("autoprefixer")()
  ]
};
